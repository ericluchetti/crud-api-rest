<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('empresas', [ApiController::class, 'index']);
Route::get('empresas/{id}', [ApiController::class, 'get']);
Route::post('empresas', [ApiController::class, 'store']);
Route::put('empresas/{id}', [ApiController::class, 'update']);
Route::delete('empresas/{id}',[ApiController::class, 'destroy']);
